CREATE PROCEDURE FO_DETTAGLIOSCARICO
(
	in object_type nvarchar(30), 				-- SBO Object Type
	in transaction_type nchar(1),			-- [A]dd, [U]pdate, [D]elete, [C]ancel, C[L]ose
	in num_of_cols_in_key int,
	in list_of_key_cols_tab_del nvarchar(255),
	in list_of_cols_val_tab_del nvarchar(255),
	inout error int,				
	inout error_message nvarchar (200)
) LANGUAGE SQLSCRIPT 
AS
scarico double;
qtaOut double;
numeroCarichi int;
primaP int;
ultimaP int;
BEGIN

--AGGIUNGERE CHIAMATA ALLA SBO_
--CALL FO_DETTAGLIOSCARICO (object_type, transaction_type, num_of_cols_in_key, list_of_key_cols_tab_del, list_of_cols_val_tab_del,error, error_message);


IF :object_type = '60' and (:transaction_type = 'A' OR :transaction_type = 'U') THEN 
	DECLARE CURSOR curScarico FOR 
		SELECT  T1."Quantity", T1."ItemCode", T1."WhsCode"
		FROM	OIGE T0
					INNER JOIN IGE1 T1 ON T0."DocEntry" = T1."DocEntry"
		WHERE	T0."DocEntry" = :list_of_cols_val_tab_del;
	
	FOR righeScarico as curScarico DO
		--scarico := righeScarico."Quantity";

		DECLARE CURSOR curCarico FOR 
			SELECT 	T1."DocEntry",
					T2."LineNum",
					IFNULL(T2."U_FO_QTAOUT" ,0) AS "QTA_OUT",
					T2."Quantity",
					(T2."Quantity"-IFNULL(T2."U_FO_QTAOUT" ,0)) AS "QTA"
			FROM 	OINM T0
						INNER JOIN OIGN T1 ON T1."DocEntry" =  T0."CreatedBy"
						INNER JOIN IGN1 T2 ON T1."DocEntry" = T2."DocEntry" 
			WHERE 	T0."ItemCode" = righeScarico."ItemCode" AND
					T0."TransType" =59 AND
					IFNULL(T2."U_FO_QTAOUT" ,0) <> T2."Quantity" and
					T1."Series" =115 
			ORDER BY 
					T0."TransNum", 
					T2."LineNum";
					
		scarico := righeScarico."Quantity";
		numeroCarichi := 0;
		FOR righeCarico as curCarico DO
			
			qtaOut := righeCarico."QTA_OUT";
			IF :scarico > 0 THEN
				numeroCarichi := :numeroCarichi + 1;
				IF righeCarico."QTA" <= :scarico THEN
					scarico := :scarico - righeCarico."QTA";
					--qtaOut := qtaOut + righeCarico."QTA";
					qtaOut := righeCarico."QTA";
				ELSE
					qtaOut := :scarico;
					scarico := 0;
				END IF;
				UPDATE IGN1 SET "U_FO_QTAOUT" = :qtaOut WHERE "DocEntry" = righeCarico."DocEntry" AND "LineNum" = righeCarico."LineNum";
				INSERT INTO "@FO_REGISTROSCARICO" ("Code", U_FO_DOCENTRYOUT, U_FO_DOCENTRYIN, U_FO_QTAOUT ) (
					SELECT "@FO_REGISTROSCARICO_S".nextval, :list_of_cols_val_tab_del, righeCarico."DocEntry", :qtaOut FROM DUMMY
				);
			END IF;
			
		END FOR;
		
		SELECT	IFNULL(MAX(U_FO_ULTIMAP),0)+1
		INTO 	primaP 
		FROM	"@FO_DETTAGLIREGISTRO" 
		WHERE 	U_FO_ITEMCODE = righeScarico."ItemCode" AND
				U_FO_WHSCODE = righeScarico."WhsCode";

		--SELECT ROUND(((:primaP + :numeroCarichi + 1) / 3),0) INTO ultimaP FROM DUMMY;
		SELECT 	CASE 
					WHEN ((:numeroCarichi + 1) / 3) > ROUND(((:numeroCarichi + 1) / 3),0) THEN 
						ROUND(((:numeroCarichi + 1) / 3),0)
					ELSE
						ROUND(((:numeroCarichi + 1) / 3),0)-1
				END 
		INTO ultimaP 
		FROM DUMMY;
		
		ultimaP := :primaP + :ultimaP;
		
		INSERT INTO "@FO_DETTAGLIREGISTRO" ("Code", U_FO_DOCENTRYOUT, U_FO_PRIMAP, U_FO_ULTIMAP, U_FO_ITEMCODE, U_FO_WHSCODE) (
			SELECT 	"@FO_DETTAGLIREGISTRO_S".nextval,:list_of_cols_val_tab_del,:primaP,:ultimaP,righeScarico."ItemCode",righeScarico."WhsCode" FROM DUMMY
		);
	END FOR;
END IF;

END;