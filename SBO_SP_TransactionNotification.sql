CREATE PROCEDURE SBO_SP_TransactionNotification
(
	in object_type nvarchar(30), 				-- SBO Object Type
	in transaction_type nchar(1),			-- [A]dd, [U]pdate, [D]elete, [C]ancel, C[L]ose
	in num_of_cols_in_key int,
	in list_of_key_cols_tab_del nvarchar(255),
	in list_of_cols_val_tab_del nvarchar(255)
)
LANGUAGE SQLSCRIPT
AS
-- Return values
rif nvarchar (200);
rif2 nvarchar (200);
cnt int;
temp_var_0 int;
ctyp nvarchar (200);
error  int;				-- Result (0 for no error)
error_message nvarchar (200); 		-- Error string to be displayed
begin

error := 0;
error_message := N'Ok';

--------------------------------------------------------------------------------------------------------------------------------

--	ADD	YOUR	CODE	HERE

--------------------------------------------------------------------------------------------------------------------------------
IF :object_type='17' AND (:transaction_type='A' or :transaction_type='U') THEN 

select 	count(*)
INTO	cnt
from	(
			select 	"DocEntry","Commission", U_AGENTE
			from	rdr1
			where 	ifnull("Commission",0) != 0 and
					ifnull(U_AGENTE,'') = '' and
					"DocEntry" = :list_of_cols_val_tab_del
			union all
			select 	"DocEntry","Commission", U_AGENTE
			from	rdr1
			where 	ifnull("Commission",0) = 0 and
					ifnull(U_AGENTE,'') != '' and
					"DocEntry" = :list_of_cols_val_tab_del
		);

	IF :cnt>0 THEN
		error := -111;
		error_message := N'Inserire sia il Codice Agente che la Percentuale';
	END IF;
	
	SELECT 	COUNT(*)
	INTO	cnt
	FROM	ORDR
	WHERE	"OwnerCode" IS NULL AND
			"DocEntry" = :list_of_cols_val_tab_del;
	
	IF :cnt > 0 THEN
		error := -111;
		error_message := N'Inserire il titolare';
	END IF;
	
END IF;

IF :object_type = '23' and (:transaction_type='A' or :transaction_type='U' or :transaction_type='D') THEN 

	SELECT 	COUNT(*)
	INTO	cnt
	FROM	OQUT
	WHERE	"OwnerCode" IS NULL AND
			"DocEntry" = :list_of_cols_val_tab_del;
	
	IF :cnt > 0 THEN
		error := -111;
		error_message := N'Inserire il titolare';
	END IF;
	
END IF;

IF :object_type = '15' and (:transaction_type='A' or :transaction_type='U') THEN 
	
	SELECT 	COUNT(*)
	INTO	cnt
	FROM	ODLN
	WHERE	U_COLLI IS NULL AND
			"DocEntry" = :list_of_cols_val_tab_del;
	
	IF :cnt > 0 THEN
		error := -111;
		error_message := N'Inserire il numero colli';
	END IF;	
END IF;

IF :object_type='13' AND (:transaction_type='A' or :transaction_type='U') THEN 
	SELECT 
		CASE
				WHEN (month(max(T0."DocDate")) <> month(min(T2."DocDate"))) OR (year(max(T0."DocDate")) <> year(min(T2."DocDate"))) THEN
					1
				ELSE
					0
		END INTO cnt
	FROM 	OINV T0  
				INNER JOIN INV1 T1 ON T0."DocEntry" = T1."DocEntry"
				INNER JOIN ODLN T2 ON T2."DocEntry" = T1."BaseEntry" AND T1."BaseType" = 15 
	WHERE 	T0."DocEntry" = :list_of_cols_val_tab_del;

	IF :cnt>0 THEN
		error := -111;
		error_message := N'Impossibile fatturare DDT di mesi precedenti.';
	END IF;
	
END IF;

IF :object_type = '4' and (:transaction_type='A' or :transaction_type='U') THEN 

	UPDATE  OITM
	SET U_FLGELA='N'
	WHERE "ItemCode" = :list_of_cols_val_tab_del;	
	
	update OITM a
	set a."U_TAGLIA" = ( --Imposta campo taglia
		select substring(
			substring(a."ItemCode",instr(a."ItemCode",'-')+1),1, 
			instr(
				substring(a."ItemCode",instr(a."ItemCode",'-')+1),'-')-1) as "Taglia"
		from dummy),
	a."U_COLORE" = ( -- Imposta campo colore
		select substring(
			substring(a."ItemCode",instr(a."ItemCode",'-')+1),
			instr(
				substring(a."ItemCode",instr(a."ItemCode",'-')+1),'-')+1) as "Colore"
		from dummy)
	where a."ItemCode" = :list_of_cols_val_tab_del;

END IF;

-- Select the return values
IF (:error = 0)
Then 
IF(:object_type !='410000000' )
Then 
IF (:object_type = '112' or :object_type = '164' or :object_type = '166' or :object_type = '163' 
 or :object_type = '10000011' or :object_type = '10000062' or :object_type = '165' or :object_type = '18' or :object_type = '16' 
 or :object_type = '14' or :object_type = '22' or :object_type = '21' or :object_type = '19' or :object_type = '15' 
 or :object_type = '13' or :object_type = '17' or :object_type = '20' or :object_type = '540000006' 
 or :object_type = '23' or :object_type = '204' or :object_type = '203' or :object_type = '234000031' 
 or :object_type = '234000032')
 Then 
CALL SBO_MOST."HUB_SP_TRANSACTIONNOTIFICATION_DOC"(object_type, transaction_type, num_of_cols_in_key, list_of_key_cols_tab_del, list_of_cols_val_tab_del,:error,:error_message);
 ELSE 
CALL SBO_MOST."HUB_SP_TRANSACTIONNOTIFICATION_MASTER"(object_type, transaction_type, num_of_cols_in_key, list_of_key_cols_tab_del, list_of_cols_val_tab_del,:error,:error_message);
 END IF;
End IF;
select :error, :error_message FROM dummy;

ELSE
select :error, :error_message FROM dummy;
END IF;
end;