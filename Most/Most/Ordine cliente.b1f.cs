
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Most
{

    [FormAttribute("139", "Ordine cliente.b1f")]
    class Ordine_cliente : SystemFormBase
    {
        public Ordine_cliente()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("38").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("12").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.EditText EditText0;
        private void OnCustomInitialize()
        {

        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            int iRigaSel = this.Matrix0.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_RowOrder);

            string itemCode = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("1").Cells.Item(iRigaSel).Specific)).Value.ToString();
            string itemName = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("3").Cells.Item(iRigaSel).Specific)).Value.ToString();
            string qtaAperta = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("32").Cells.Item(iRigaSel).Specific)).Value.ToString();
            string docEntry = this.UIAPIRawForm.DataSources.DBDataSources.Item("ORDR").GetValue("DocEntry", 0);
            string dtCons = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("25").Cells.Item(iRigaSel).Specific)).Value.ToString();
            GeneraOP activeForm = new GeneraOP(docEntry, qtaAperta, itemCode, itemName, dtCons);
            activeForm.Show();

        }

        private SAPbouiCOM.Matrix Matrix0;

        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            if (this.UIAPIRawForm.Mode != SAPbouiCOM.BoFormMode.fm_OK_MODE)
            {
                Application.SBO_Application.MessageBox("Salvare i dati prima di generare un Ordine di Produzione.");
                BubbleEvent = false;
                return;
            }

            int iRigaSel = this.Matrix0.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_RowOrder);
            if (iRigaSel == -1)
            {
                Application.SBO_Application.MessageBox("Selezionare una riga");
                BubbleEvent = false;
                return;
            }

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string itemCode = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("1").Cells.Item(iRigaSel).Specific)).Value.ToString();

            oRecordset.DoQuery("SELECT T0.\"ItmsGrpCod\" FROM OITM T0 WHERE T0.\"ItemCode\" ='" + itemCode + "' ");
            if ( oRecordset.Fields.Item(0).Value.ToString() != "112")
	        {
                Application.SBO_Application.MessageBox("Si possono generare Ordini di Produzione solo di articoli del gruppo PRODOTTI FINITI.");
                BubbleEvent = false;
                return;		 
	        }
            

        }

    }
}
