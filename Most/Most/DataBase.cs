﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManager;

namespace Most
{
    class DataBase : Db
    {
        public DataBase()
        {
            Tables = new DbTable[] {
                new DbTable("@ADDONPAR", "Parametri AddOn", SAPbobsCOM.BoUTBTableType.bott_NoObject),

                
                new DbTable("@FO_ORDPROD", "Ordine di Produzione", SAPbobsCOM.BoUTBTableType.bott_Document),

            };

            Columns = new DbColumn[] {
                
                #region FOADDONPAR Parametri addon
                new DbColumn("@ADDONPAR", "VALOREDEC", "Valore decimale", 
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 10, new DbValidValue[0],-1,null),
                new DbColumn("@ADDONPAR", "VALOREPRC", "Valore %", 
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 10, new DbValidValue[0],-1,null),
                
                #endregion

                new DbColumn("@FO_ORDPROD", "FO_NOTA", "Nota", 
                    SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, new DbValidValue[0],-1,null),
                new DbColumn("@FO_ORDPROD", "FO_QTA", "Quantità", 
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10, new DbValidValue[0],-1,null),

            };


        }
    }
}
