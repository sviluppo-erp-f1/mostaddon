﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Most
{
    [FormAttribute("Most.GeneraOP", "GeneraOP.b1f")]
    class GeneraOP : UserFormBase
    {
        string docEntryOrdine;
        string qta;
        string itemCode;
        string itemName;
        string dtCons;
        public GeneraOP(string docEntryOrdineP, string qtaP, string itemCodeP, string itemNameP, string dtConsP)
        {
            docEntryOrdine = docEntryOrdineP;
            qta = qtaP;
            itemCode = itemCodeP;
            itemName = itemNameP;
            dtCons = dtConsP;
            
            this.EditText2.Value = qta;
            this.EditText4.Value = itemCode;
            this.EditText5.Value = itemName;

            this.EditText1.Item.Click(SAPbouiCOM.BoCellClickType.ct_Regular);
            this.EditText4.Item.Enabled=false;
            this.EditText5.Item.Enabled=false;
            
            
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_0").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_6").Specific));
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_7").Specific));
            this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_8").Specific));
            this.Button2.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button2_ClickAfter);
            this.Button2.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button2_ClickBefore);
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_9").Specific));
            this.StaticText3 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_10").Specific));
            this.EditText4 = ((SAPbouiCOM.EditText)(this.GetItem("Item_11").Specific));
            this.EditText5 = ((SAPbouiCOM.EditText)(this.GetItem("Item_12").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {

        }

        

        private void OnCustomInitialize()
        {
            inizializzaStampa();

            this.EditText0.Item.AffectsFormMode = false;
            this.EditText1.Item.AffectsFormMode = false;
            this.EditText2.Item.AffectsFormMode = false;
            this.EditText4.Item.AffectsFormMode = false;
            this.EditText5.Item.AffectsFormMode = false;

            this.UIAPIRawForm.EnableMenu("1281", false);
            this.UIAPIRawForm.EnableMenu("1282", false);

        }

        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.Button Button2;
        private SAPbouiCOM.StaticText StaticText2;
        private SAPbouiCOM.StaticText StaticText3;
        private SAPbouiCOM.EditText EditText4;
        private SAPbouiCOM.EditText EditText5;

        public static string getDocEntry()
        {
            return ((SAPbouiCOM.EditText)(Application.SBO_Application.Forms.ActiveForm.Items.Item("Item_0").Specific)).Value.ToString();
        }

        private void inizializzaStampa()
        {
            

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            oRecordset.DoQuery("SELECT T0.\"DEFLT_REP\" FROM RTYP T0 WHERE T0.\"CODE\" ='WOR1'");

            SAPbobsCOM.ReportTypesService rptTypeService = (SAPbobsCOM.ReportTypesService)Program.oCompany.GetCompanyService().GetBusinessService(SAPbobsCOM.ServiceTypes.ReportTypesService);

            while (!oRecordset.EoF)
            {
                SAPbobsCOM.ReportTypeParams rptParams = (SAPbobsCOM.ReportTypeParams)
                rptTypeService.GetDataInterface(SAPbobsCOM.ReportTypesServiceDataInterfaces.rtsReportTypeParams);
                rptParams.TypeCode = "WOR1";
                SAPbobsCOM.ReportType updateType = rptTypeService.GetReportType(rptParams);
                updateType.DefaultReportLayout = oRecordset.Fields.Item(0).Value.ToString();
                rptTypeService.UpdateReportType(updateType);
                
                this.UIAPIRawForm.ReportType = "WOR1";

                oRecordset.MoveNext();
            }

        }

        private void Button2_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            if (!this.Button2.Item.Enabled)
            {
                BubbleEvent = false;
                return;
            }

            if (this.EditText2.Value.ToString()=="")
            {
                Application.SBO_Application.MessageBox("Inserire la quantità");
                BubbleEvent = false;
            }

            if (double.Parse(this.EditText2.Value.ToString().Replace(".",",")) <= 0)
            {
                Application.SBO_Application.MessageBox("La quantità deve essere maggiore di 0");
                BubbleEvent = false;
            }

            if (this.EditText1.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Inserire la nota");
                BubbleEvent = false;
            }


        }

        private void Button2_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            SAPbobsCOM.ProductionOrders oPO = (SAPbobsCOM.ProductionOrders)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
            string project = "";
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //Estraggo il progetto
            oRecordset.DoQuery("SELECT T0.U_IMPIANTO FROM OITM T0 WHERE T0.\"ItemCode\" ='" + itemCode + "'");
            project = oRecordset.Fields.Item(0).Value.ToString();

            oPO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotSpecial;
            oPO.ItemNo = itemCode;
            oPO.PlannedQuantity = double.Parse(this.EditText2.Value.ToString().Replace(".", ","));
            oPO.ProductionOrderOriginEntry = int.Parse(docEntryOrdine);
            oPO.Project = project;
            oPO.UserFields.Fields.Item("U_PROTOTIPIA").Value = "Y";

            // oPO.DueDate = DateTime.ParseExact(dtCons, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
             oRecordset = Program.oSBObob.Format_StringToDate(dtCons);
             oPO.DueDate = Convert.ToDateTime(oRecordset.Fields.Item(0).Value.ToString());

            oPO.Stages.StageEntry = 1;
            oPO.Stages.SequenceNumber = 1;

            oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Resource;
            oPO.Lines.ItemNo = "Prototipia manuale";
            //oPO.Lines.BaseQuantity = double.Parse(this.EditText2.Value.ToString().Replace(".", ","));
            oPO.Lines.StageID = 1;
            oPO.Lines.Add();

            oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Text;
            oPO.Lines.LineText = this.EditText1.Value.ToString();
            oPO.Lines.StageID = 1;
            oPO.Lines.Add();

            try
            {
                int res = oPO.Add();
                if (res == 0)
                {
                    string docEntry;
                    Program.oCompany.GetNewObjectCode(out docEntry);
                    this.EditText0.Value = docEntry;

                    oPO = (SAPbobsCOM.ProductionOrders)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                    oPO.GetByKey(int.Parse(docEntry));
                    this.StaticText2.Caption = "Generato Ordine di Produzione numero: " + oPO.DocumentNumber;

                    this.Button1.Item.Enabled = true;

                    oPO.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased;
                    oPO.Update();

                    this.Button2.Item.Enabled = false;
                }
                else
                {
                    Application.SBO_Application.MessageBox("ERRORE sulla generazione Ordinedi Produzione: " + Program.oCompany.GetLastErrorDescription());
                }
            }
            catch (Exception)
            {
                
                
            }
            
        }

        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            
            Application.SBO_Application.ActivateMenuItem("519");
        }

        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!this.Button1.Item.Enabled)
            {
                BubbleEvent = false;
                return;
            }
        }

    }
}
