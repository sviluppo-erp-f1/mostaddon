
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Most
{

    [FormAttribute("65211", "Ordine di produzione.b1f")]
    class Ordine_di_produzione : SystemFormBase
    {
        public Ordine_di_produzione()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_0").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_2").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {
            if (this.UIAPIRawForm.Width < 970)
            {
                this.UIAPIRawForm.Width = 970;
            }

            SAPbouiCOM.Item oItem = this.UIAPIRawForm.Items.Add("RECT", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE);
            this.EditText0.Item.AffectsFormMode = false;
            this.EditText1.Item.AffectsFormMode = false;
        }

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            #region Rettangolo
            this.UIAPIRawForm.Items.Item("RECT").Left = this.GetItem("6").Left + this.GetItem("6").Width + 30;
            this.UIAPIRawForm.Items.Item("RECT").Top = this.GetItem("6").Top - 10;
            this.UIAPIRawForm.Items.Item("RECT").Width = 270;
            this.UIAPIRawForm.Items.Item("RECT").Height = 50;
            #endregion

            this.StaticText2.Item.Left = this.UIAPIRawForm.Items.Item("RECT").Left;
            this.StaticText2.Item.Top = this.UIAPIRawForm.Items.Item("RECT").Top - 15;
            #region Articolo
            this.StaticText0.Item.Left = this.UIAPIRawForm.Items.Item("RECT").Left + 5;
            this.StaticText0.Item.Top = this.UIAPIRawForm.Items.Item("RECT").Top + 5;
            this.EditText0.Item.Left = this.StaticText0.Item.Left + this.StaticText0.Item.Width + 3;
            this.EditText0.Item.Top = this.StaticText0.Item.Top;
            #endregion

            #region Quantità
            this.StaticText1.Item.Left = this.StaticText0.Item.Left;
            this.StaticText1.Item.Top = this.GetItem("8").Top;
            this.EditText1.Item.Left = this.StaticText1.Item.Left + this.StaticText1.Item.Width + 3;
            this.EditText1.Item.Top = this.StaticText1.Item.Top;
            #endregion

            #region OK
            this.Button0.Item.Top = this.StaticText1.Item.Top;
            this.Button0.Item.Left = (this.EditText0.Item.Left + this.EditText0.Item.Width) - this.Button0.Item.Width;
            #endregion

        }

        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (this.UIAPIRawForm.Mode == SAPbouiCOM.BoFormMode.fm_FIND_MODE)
            {
                BubbleEvent = false;
                return;
            }
            //if (this.UIAPIRawForm.Mode != SAPbouiCOM.BoFormMode.fm_OK_MODE)
            //{
            //    Application.SBO_Application.MessageBox("E' necessario salvare i dati.");
            //    BubbleEvent = false;
            //    return;
            //}


            if (this.EditText0.Value.ToString() =="")
            {
                Application.SBO_Application.MessageBox("Scegliere il nuovo articolo da creare.");
                BubbleEvent = false;
                return;
            }
            if (this.EditText1.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Inserire la quantità del nuovo ordine di produzione.");
                BubbleEvent = false;
                return;
            }

            if (((SAPbouiCOM.ComboBox)(this.GetItem("20").Specific)).Value.ToString() != "P")
            {
                Application.SBO_Application.MessageBox("Si possono duplicare soltanto ordini di tipo Speciale.");
                BubbleEvent = false;
                return;
            }

        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Freeze(true);
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            Application.SBO_Application.OpenForm(SAPbouiCOM.BoFormObjectEnum.fo_ProductionOrder, null, null);
            SAPbouiCOM.Form oForm = Application.SBO_Application.Forms.ActiveForm;
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;

            ((SAPbouiCOM.ComboBox)(oForm.Items.Item("20").Specific)).Select("P", SAPbouiCOM.BoSearchKey.psk_ByValue); //Tipo
            ((SAPbouiCOM.EditText)(oForm.Items.Item("6").Specific)).Value = this.EditText0.Value.ToString(); //Articolo da produrre
            ((SAPbouiCOM.EditText)(oForm.Items.Item("12").Specific)).Value = this.EditText1.Value.ToString(); //Quantità

            ((SAPbouiCOM.EditText)(oForm.Items.Item("78").Specific)).Value = ((SAPbouiCOM.EditText)(this.GetItem("78").Specific)).Value.ToString(); //Magazzino 

            ((SAPbouiCOM.EditText)(oForm.Items.Item("255000122").Specific)).Value = ((SAPbouiCOM.EditText)(this.GetItem("255000122").Specific)).Value.ToString(); //Priorità
            ((SAPbouiCOM.EditText)(oForm.Items.Item("540000153").Specific)).Value = ((SAPbouiCOM.EditText)(this.GetItem("540000153").Specific)).Value.ToString(); //Progetto

            oRecordset.DoQuery("SELECT count(*) FROM ORDR T0 WHERE T0.\"DocNum\" ='" + ((SAPbouiCOM.EditText)(this.GetItem("32").Specific)).Value.ToString() + "' and  T0.\"DocStatus\" !='C' and  T0.\"CANCELED\" ='N' ");
            if (oRecordset.Fields.Item(0).Value.ToString() != "0")
	        {
                ((SAPbouiCOM.EditText)(oForm.Items.Item("32").Specific)).Value = ((SAPbouiCOM.EditText)(this.GetItem("32").Specific)).Value.ToString(); //Ordine cliente
	        }

            oRecordset = Program.oSBObob.Format_StringToDate(((SAPbouiCOM.EditText)(this.GetItem("26").Specific)).Value.ToString());
            if (Convert.ToDateTime(oRecordset.Fields.Item(0).Value.ToString()) > DateTime.Now.Date)
            {
                ((SAPbouiCOM.EditText)(oForm.Items.Item("26").Specific)).Value = ((SAPbouiCOM.EditText)(this.GetItem("26").Specific)).Value.ToString(); //Data scadenza
            }
            

            SAPbouiCOM.Matrix oMatrix = ((SAPbouiCOM.Matrix)(this.GetItem("37").Specific));
            SAPbouiCOM.Matrix oMatrixD = ((SAPbouiCOM.Matrix)(oForm.Items.Item("37").Specific));

            for (int x = 1; x <= oMatrix.RowCount; x++)
            {
                int row = oMatrixD.RowCount;
                string val = ((SAPbouiCOM.ComboBox)(oMatrix.Columns.Item("1880000002").Cells.Item(x).Specific)).Value.ToString();
                ((SAPbouiCOM.ComboBox)(oMatrixD.Columns.Item("1880000002").Cells.Item(row).Specific)).Select(val,SAPbouiCOM.BoSearchKey.psk_ByValue); //Tipo operzione

                //Se sto inserendo un testo
                if (val =="-18") 
                {
                    //Riga di testo
                    ((SAPbouiCOM.EditText)(Application.SBO_Application.Forms.ActiveForm.Items.Item("5").Specific)).Value = ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("1880000006").Cells.Item(x).Specific)).Value.ToString();
                    Application.SBO_Application.Forms.ActiveForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular);
                }
                else
                {
                    //Codice articolo o risorsa
                    ((SAPbouiCOM.EditText)(oMatrixD.Columns.Item("4").Cells.Item(row).Specific)).Value = ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("4").Cells.Item(x).Specific)).Value.ToString();

                    double qta = 0;
                    if (val=="4")
                    {
                        //Quantità articolo
                        qta = ((double.Parse(((SAPbouiCOM.EditText)(oMatrix.Columns.Item("2").Cells.Item(x).Specific)).Value.ToString().Replace(".", ",")) / double.Parse(((SAPbouiCOM.EditText)(this.GetItem("12").Specific)).Value.ToString().Replace(".", ","))) * double.Parse(this.EditText1.Value.ToString().Replace(".", ",")));
                    }
                    if (val == "290")
                    {
                        //Quantità risorsa
                        qta = double.Parse(((SAPbouiCOM.EditText)(oMatrix.Columns.Item("2").Cells.Item(x).Specific)).Value.ToString().Replace(".", ","));
                    }

                    if (qta != 0)
                    {
                        ((SAPbouiCOM.EditText)(oMatrixD.Columns.Item("2").Cells.Item(row).Specific)).Value = qta.ToString().Replace(",",".");  
                    
                        //Magazzino
                        ((SAPbouiCOM.EditText)(oMatrixD.Columns.Item("10").Cells.Item(row).Specific)).Value = ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("10").Cells.Item(row).Specific)).Value.ToString(); 

                    }

                }

            }
            this.UIAPIRawForm.Freeze(false);
        }

        private SAPbouiCOM.StaticText StaticText2;




    }
}
