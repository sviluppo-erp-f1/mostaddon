﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManager;

namespace DistinteBase
{
    class DataBase : Db
    {
        public DataBase()
        {
            Tables = new DbTable[] {
                new DbTable("@ADDONPAR", "Parametri AddOn", SAPbobsCOM.BoUTBTableType.bott_NoObject),

            };

            Columns = new DbColumn[] {
                
                #region FOADDONPAR Parametri addon
                new DbColumn("@ADDONPAR", "VALOREDEC", "Valore decimale", 
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 10, new DbValidValue[0],-1,null),
                new DbColumn("@ADDONPAR", "VALOREPRC", "Valore %", 
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 10, new DbValidValue[0],-1,null),
                
                #endregion

                #region OWOR
                new DbColumn("OWOR", "FO_OPG", "Ordine Prod. di Gruppo", 
                    SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10, new DbValidValue[0],-1,null),
                #endregion

            };
        }
    }
}
