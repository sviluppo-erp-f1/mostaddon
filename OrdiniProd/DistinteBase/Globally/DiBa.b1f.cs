﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace DistinteBase
{
    [FormAttribute("DistinteBase.DiBa", "DiBa.b1f")]
    class DiBa : UserFormBase
    {
        private SAPbouiCOM.ComboBox ComboBox0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.StaticText StaticText2;
        private SAPbouiCOM.ComboBox ComboBox1;
        private SAPbouiCOM.StaticText StaticText3;
        private SAPbouiCOM.StaticText StaticText4;
        private SAPbouiCOM.EditText EditText3;
        private SAPbouiCOM.StaticText StaticText5;
        private SAPbouiCOM.EditText EditText5;
        private SAPbouiCOM.Matrix Matrix0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;
        private SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        private SAPbouiCOM.ComboBox ComboBox2;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.StaticText StaticText6;
        private SAPbouiCOM.ComboBox ComboBox3;
        private bool loadFromLink;

        public DiBa()
        {
        }

        public override void OnInitializeComponent()
        {
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_2").Specific));
            this.ComboBox0.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox0_ComboSelectAfter);
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            this.ComboBox1 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_6").Specific));
            this.ComboBox1.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox1_ComboSelectAfter);
            this.ComboBox1.KeyDownBefore += new SAPbouiCOM._IComboBoxEvents_KeyDownBeforeEventHandler(this.ComboBox1_KeyDownBefore);
            this.StaticText3 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_7").Specific));
            this.StaticText4 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_10").Specific));
            this.EditText3 = ((SAPbouiCOM.EditText)(this.GetItem("Item_11").Specific));
            this.StaticText5 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_12").Specific));
            this.EditText5 = ((SAPbouiCOM.EditText)(this.GetItem("Item_14").Specific));
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("Item_15").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_16").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_17").Specific));
            this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.ComboBox2 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_18").Specific));
            this.ComboBox2.KeyDownBefore += new SAPbouiCOM._IComboBoxEvents_KeyDownBeforeEventHandler(this.ComboBox2_KeyDownBefore);
            this.StaticText6 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_19").Specific));
            this.ComboBox3 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_20").Specific));
            this.StaticText7 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_1").Specific));
            this.ComboBox4 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_8").Specific));
            this.OnCustomInitialize();

        }

        public override void OnInitializeFormEvents()
        {
            this.LoadAfter += new SAPbouiCOM.Framework.FormBase.LoadAfterHandler(this.Form_LoadAfter);
            this.DataLoadAfter += new SAPbouiCOM.Framework.FormBase.DataLoadAfterHandler(this.Form_DataLoadAfter);
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private void OnCustomInitialize()
        {
            PopolaComboBrand();
        }

        private void PopolaComboBrand()
        {
            ComboBox0.ValidValues.Add("", "");
            oRecordset.DoQuery("SELECT T0.\"Code\", T0.\"Name\" FROM \"@F1_BRANDS\" T0 ORDER BY T0.\"Name\"");
            while (!oRecordset.EoF)
            {
                ComboBox0.ValidValues.Add(oRecordset.Fields.Item(0).Value.ToString(), oRecordset.Fields.Item(1).Value.ToString());
                oRecordset.MoveNext();
            }
        }

        private void PopolaComboProgetto()
        {
            if (ComboBox0.Value.ToString() != "")
            {
                while (ComboBox1.ValidValues.Count != 0)
                {
                    ComboBox1.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
                ComboBox1.ValidValues.Add("", "");
                string brand = ComboBox0.Value.ToString();
                oRecordset.DoQuery("SELECT DISTINCT(T0.\"U_IMPIANTO\") FROM \"OITM\" T0 WHERE T0.\"U_BRAND\" = '" + brand + "' AND T0.\"U_IMPIANTO\" <> '' ORDER BY T0.\"U_IMPIANTO\" ");
                while (!oRecordset.EoF)
                {
                    ComboBox1.ValidValues.Add(oRecordset.Fields.Item(0).Value.ToString(), oRecordset.Fields.Item(0).Value.ToString());
                    oRecordset.MoveNext();
                }

                ComboBox1.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            }
        }
        
        private void PopolaComboOrdini()
        {
            if (ComboBox0.Value.ToString() != "" && ComboBox1.Value.ToString() != "" )
            {
                while (ComboBox2.ValidValues.Count != 0)
                {
                    ComboBox2.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
                ComboBox2.ValidValues.Add("", "");
                string progetto = ComboBox1.Value.ToString();
                oRecordset.DoQuery("SELECT DISTINCT(T0.\"DocNum\"), T0.\"DocEntry\"  FROM ORDR T0 INNER JOIN RDR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" INNER JOIN OITM T2 ON T1.\"ItemCode\" = T2.\"ItemCode\" WHERE T2.\"U_IMPIANTO\"  = '" + progetto + "' AND  T0.\"DocStatus\"  = 'O' ORDER BY T0.\"DocNum\" DESC ");
                while (!oRecordset.EoF)
                {
                    ComboBox2.ValidValues.Add(oRecordset.Fields.Item(1).Value.ToString(), oRecordset.Fields.Item(0).Value.ToString());
                    oRecordset.MoveNext();
                }
                ComboBox2.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                
            }
        }

        private void PopolaMatrixArticoli()
        {
            string HanaQuery = "";
            HanaQuery += "SELECT T0.\"ItemCode\", T0.\"ItemName\", T3.\"Father\", NULL AS \"Qta\", T4.\"UomName\" FROM OITM T0 INNER JOIN RDR1 T1 ON T0.\"ItemCode\" = T1.\"ItemCode\" ";
            HanaQuery += " INNER JOIN ORDR T2 ON T1.\"DocEntry\" = T2.\"DocEntry\" LEFT JOIN ITT1 T3 ON T0.\"ItemCode\" = T3.\"Father\" LEFT JOIN OUOM T4 ON T0.\"PriceUnit\" = T4.\"UomEntry\" ";
            HanaQuery += " WHERE T2.\"DocEntry\"  = '" + ComboBox2.Value.ToString() + "' AND  T0.\"U_IMPIANTO\" = '" + ComboBox1.Value.ToString() + "' ";
            HanaQuery += " GROUP BY T0.\"ItemCode\", T0.\"ItemName\", T3.\"Father\", T0.\"U_IMPIANTO\",T0.\"U_COLORE\", T0.\"U_TAGLIA\", T4.\"UomName\" ";
            HanaQuery +=  "ORDER BY T0.\"U_IMPIANTO\", T0.\"U_COLORE\", T0.\"U_TAGLIA\" ";

            this.UIAPIRawForm.Freeze(true);
            this.UIAPIRawForm.DataSources.DataTables.Item("DT_ART").Clear();
            this.UIAPIRawForm.DataSources.DataTables.Item("DT_ART").ExecuteQuery(HanaQuery);
            this.Matrix0.Columns.Item("ItemCode").DataBind.Bind("DT_ART", "ItemCode");
            this.Matrix0.Columns.Item("ItemName").DataBind.Bind("DT_ART", "ItemName");
            this.Matrix0.Columns.Item("DiBa").DataBind.Bind("DT_ART", "Father");
            this.Matrix0.Columns.Item("Unit").DataBind.Bind("DT_ART", "UomName");
            this.Matrix0.LoadFromDataSource();
            this.Matrix0.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);
        }

        private void CreaOP()
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            int errore = 0;
            //CHIAMA SEQUENCE
            oRecordset.DoQuery("SELECT SEQ_OPG.NEXTVAL FROM DUMMY ");
            int ID_OPG = int.Parse(oRecordset.Fields.Item(0).Value.ToString());
            //SE ORDINE STANDARD
            if (this.ComboBox3.Value.ToString() == "std")
            {
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    if (double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString()) > 0)
                    {
                        //Controllo che esistano delle DIBA
                        if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("DiBa").Cells.Item(i).Specific)).Value.ToString() != "")
                        {
                            SAPbobsCOM.ProductionOrders oPO = (SAPbobsCOM.ProductionOrders)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                            
                            double Qta = double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString().Replace(".", ",")); ///DA CONTROLLARE
                            string ItemCode = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("ItemCode").Cells.Item(i).Specific)).Value.ToString();
                            //Estraggo il progetto
                            oRecordset.DoQuery("SELECT T0.U_IMPIANTO FROM OITM T0 WHERE T0.\"ItemCode\" ='" + ItemCode + "'");
                            string project = oRecordset.Fields.Item(0).Value.ToString();
                            //oPO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotStandard;
                            oPO.ItemNo = ItemCode;
                            oPO.PlannedQuantity = Qta;
                            oPO.DueDate = DateTime.ParseExact(this.EditText1.Value.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                            oPO.Project = project;
                            oPO.UserFields.Fields.Item("U_BOLLA").Value = this.EditText3.Value.ToString();
                            oPO.UserFields.Fields.Item("U_PROTOTIPIA").Value = "N";
                            oPO.UserFields.Fields.Item("U_FO_OPG").Value = ID_OPG;

                            oPO.ProductionOrderOriginEntry = int.Parse(this.ComboBox2.Value.ToString());
                            int linecount = oPO.Lines.Count;

                            int res = oPO.Add();
                            if (res == 0)
                            {
                                string docEntry = Program.oCompany.GetNewObjectKey();
                                oPO.GetByKey(int.Parse(docEntry));
                                if (this.EditText5.Value.ToString() != "" || ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Note").Cells.Item(i).Specific)).Value.ToString() != "")
                                {
                                    oPO.Lines.Add();
                                }
                                if (this.EditText5.Value.ToString() != "")
                                {
                                    oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Text;
                                    oPO.Lines.LineText = this.EditText5.Value.ToString();
                                    //oPO.Lines.StageID = 1;
                                    oPO.Lines.Add();
                                }
                                if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Note").Cells.Item(i).Specific)).Value.ToString() != "")
                                {
                                    oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Text;
                                    oPO.Lines.LineText = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Note").Cells.Item(i).Specific)).Value.ToString();
                                    //oPO.Lines.StageID = 1;
                                    oPO.Lines.Add();
                                }
                                int resu = oPO.Update();
                            }
                            else
                            {
                                Application.SBO_Application.MessageBox("Errore inserimento Ordine Produzione articolo: " + ItemCode + " errore: " + Program.oCompany.GetLastErrorDescription());
                            }
                        }
                        else
                        {
                            errore = 1;
                        }
                    
                    }
                }
                if (errore == 1)
                {
                    Application.SBO_Application.MessageBox("Attenzione, Mancano delle distinte base per gli articoli selezionati");
                }
            }
            else if (this.ComboBox3.Value.ToString() == "spe")
            {
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    if (double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString()) > 0)
                    {
                        SAPbobsCOM.ProductionOrders oPO = (SAPbobsCOM.ProductionOrders)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                        double Qta = double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString().Replace(".", ","));
                        string ItemCode = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("ItemCode").Cells.Item(i).Specific)).Value.ToString();
                        //Estraggo il progetto
                        oRecordset.DoQuery("SELECT T0.U_IMPIANTO FROM OITM T0 WHERE T0.\"ItemCode\" ='" + ItemCode + "'");
                        string project = oRecordset.Fields.Item(0).Value.ToString();
                        oPO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotSpecial;
                        oPO.ItemNo = ItemCode;
                        oPO.PlannedQuantity = Qta;
                        oPO.ProductionOrderOriginEntry = int.Parse(this.ComboBox2.Value.ToString());
                        oPO.DueDate = DateTime.ParseExact(this.EditText1.Value.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        oPO.Project = project;
                        oPO.UserFields.Fields.Item("U_PROTOTIPIA").Value = this.ComboBox4.Value.ToString();
                        oPO.UserFields.Fields.Item("U_BOLLA").Value = this.EditText3.Value.ToString();
                        oPO.UserFields.Fields.Item("U_FO_OPG").Value = ID_OPG;

                        oPO.Stages.StageEntry = 1;
                        oPO.Stages.SequenceNumber = 1;
                        oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Resource;
                        oPO.Lines.ItemNo = "Prototipia manuale";
                        oPO.Lines.StageID = 1;
                        oPO.Lines.Add();
                        oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Text;
                        oPO.Lines.LineText = this.EditText5.Value.ToString();
                        oPO.Lines.StageID = 1;
                        oPO.Lines.Add();

                        if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Note").Cells.Item(i).Specific)).Value.ToString() != "")
                        {
                            oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Text;
                            oPO.Lines.LineText = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Note").Cells.Item(i).Specific)).Value.ToString();
                            oPO.Lines.StageID = 1;
                            oPO.Lines.Add();
                        }

                        int res = oPO.Add();
                        if (res == 0)
                        { 

                        }
                        else
                        {
                            Application.SBO_Application.MessageBox("Errore inserimento Ordine Produzione articolo: " + ItemCode + " errore: " + Program.oCompany.GetLastErrorDescription());
                            errore = 1;
                        }
                    }
                }
            }
            else
            {
                Application.SBO_Application.MessageBox("Attenzione, Inserire tipo ordine produzione");
                errore = 1;
            }

            if (errore == 0)
            {
                ClearForm();
                Application.SBO_Application.MessageBox("Completato, Elaborazione completata con successo");
            }
        }

        private bool CheckDati()
        {

            if (this.ComboBox0.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Attenzione, Campo Brand obbligatorio");
                return false;
            }
            if (this.EditText1.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Attenzione, Campo Data Consegna obbligatorio");
                return false;
            }
            if (this.ComboBox1.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Attenzione, Campo Progetto obbligatorio");
                return false;
            }
            if (this.ComboBox2.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Attenzione, Campo Ordine Cliente obbligatorio");
                return false;
            }
            if (this.ComboBox3.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Attenzione, Campo Tipo obbligatorio");
                return false;
            }
            if (this.ComboBox3.Value.ToString() == "spe")
            {
                if (this.EditText5.Value.ToString() == "")
                {
                    Application.SBO_Application.MessageBox("Attenzione, Campo Note obbligatorio con tipo Speciale");
                    return false;
                }
            }

            if (this.ComboBox3.Value.ToString() == "std")
            {
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    if (double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString()) > 0)
                    {
                        if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("DiBa").Cells.Item(i).Specific)).Value.ToString() == "")
                        {
                            Application.SBO_Application.MessageBox("Attenzione, Mancano delle distinte base per gli articoli selezionati");
                            return false;
                        }
                    }
                }
                if (this.ComboBox4.Value.ToString() == "Y")
                {
                    Application.SBO_Application.MessageBox("Attenzione, impossibile generare ordine prototipia con tipo Standard");
                    return false;
                }
            }
            return true;
        }

        private void ClearForm()
        {
            this.EditText1.Value = "";
            this.EditText3.Value = "";
            this.EditText5.Value = "";
            this.ComboBox0.Select("", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            this.ComboBox1.Select("", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            this.ComboBox3.Select("", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            this.ComboBox2.Select("", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            this.Matrix0.Clear();
        }

        private void ComboBox1_KeyDownBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            if (ComboBox0.Value.ToString() != "")
            {
                BubbleEvent = true;
            }
            else
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima il Brand", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                BubbleEvent = false;
            }
        }

        private void ComboBox2_KeyDownBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            if (ComboBox1.Value.ToString() != "")
            {
                BubbleEvent = true;
            }
            else
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima il Progetto", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                BubbleEvent = false;
            }
        }

        private void ComboBox0_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            PopolaComboProgetto();
        }

        private void ComboBox1_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            PopolaComboOrdini();
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (this.ComboBox0.Value.ToString() == "")
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima il brand", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
            }
            else if (this.ComboBox1.Value.ToString() == "")
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima il Progetto", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
            }
            else if (this.ComboBox2.Value.ToString() == "")
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima l'ordine cliente", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
            }
            else
            {
                PopolaMatrixArticoli();
            }
        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (loadFromLink)
            {
                loadFromLink = false;
                PopolaComboBrand();
            }
        }

        private void Form_DataLoadAfter(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {
            loadFromLink = true;
            if (!Object.ReferenceEquals(null, this.Matrix0))
            {
                PopolaComboBrand();
            }

        }

        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            CreaOP();
        }

        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = CheckDati();
        }

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Freeze(true);

            //margini
            int margine_top = 10;
            int margine_bottom = 10;
            int margine_left = 10;
            int margine_right = 10;
            int spaz_vert = 6;
            int spaz_oriz = 10;

            this.StaticText0.Item.Top = margine_top;
            this.StaticText0.Item.Left = margine_left;
            this.ComboBox0.Item.Left = margine_left + this.StaticText0.Item.Width;
            this.ComboBox0.Item.Top = margine_top;

            this.StaticText2.Item.Top = this.StaticText0.Item.Top + spaz_vert + this.StaticText0.Item.Height;
            this.StaticText2.Item.Left = margine_left;
            this.ComboBox1.Item.Left = margine_left + this.StaticText2.Item.Width;
            this.ComboBox1.Item.Top = this.StaticText2.Item.Top;

            this.StaticText6.Item.Top = this.StaticText2.Item.Top + spaz_vert + this.StaticText2.Item.Height;
            this.StaticText6.Item.Left = margine_left;
            this.ComboBox3.Item.Left = margine_left + this.StaticText6.Item.Width;
            this.ComboBox3.Item.Top = this.StaticText6.Item.Top;

            this.StaticText3.Item.Top = this.StaticText2.Item.Top;
            this.StaticText3.Item.Left = this.ComboBox1.Item.Left + this.ComboBox1.Item.Width + spaz_vert;
            this.ComboBox2.Item.Top = this.StaticText2.Item.Top;
            this.ComboBox2.Item.Left = this.StaticText3.Item.Left + this.StaticText3.Item.Width;

            this.StaticText7.Item.Top = this.StaticText6.Item.Top;
            this.StaticText7.Item.Left = this.StaticText3.Item.Left;
            this.ComboBox4.Item.Top = this.StaticText7.Item.Top;
            this.ComboBox4.Item.Left = this.StaticText7.Item.Left + this.StaticText7.Item.Width;

            this.StaticText4.Item.Top = this.StaticText7.Item.Top;
            this.StaticText4.Item.Left = this.ComboBox4.Item.Left + this.ComboBox4.Item.Width + spaz_vert;
            this.EditText3.Item.Top = this.StaticText7.Item.Top;
            this.EditText3.Item.Left = this.StaticText4.Item.Left + this.StaticText4.Item.Width;
            this.EditText3.Item.Width = this.UIAPIRawForm.Width - this.StaticText4.Item.Left - this.StaticText4.Item.Width - margine_left - margine_left;

            this.EditText1.Item.Top = this.StaticText0.Item.Top;
            this.EditText1.Item.Left = this.UIAPIRawForm.Width - this.EditText1.Item.Width - margine_left - margine_left;
            this.StaticText1.Item.Width = this.StaticText0.Item.Width;
            this.StaticText1.Item.Top = this.StaticText0.Item.Top ;
            this.StaticText1.Item.Left = this.EditText1.Item.Left - this.StaticText1.Item.Width;

            this.StaticText5.Item.Left = margine_left;
            this.StaticText5.Item.Top = this.StaticText6.Item.Top + spaz_vert + this.StaticText6.Item.Height;
            this.EditText5.Item.Left = this.StaticText5.Item.Left + this.StaticText5.Item.Width;
            this.EditText5.Item.Top = this.StaticText5.Item.Top;
            this.EditText5.Item.Height = this.EditText3.Item.Height * 3;
            this.EditText5.Item.Width = this.UIAPIRawForm.Width - this.StaticText5.Item.Left - this.StaticText5.Item.Width - margine_left - margine_left;

            this.Button0.Item.Width = this.StaticText0.Item.Width;
            this.Button0.Item.Top = this.EditText5.Item.Top + this.EditText5.Item.Height + spaz_vert ;
            this.Button0.Item.Left = this.UIAPIRawForm.Width - this.Button0.Item.Width - margine_left - margine_left;

            this.Matrix0.Item.Top = this.Button0.Item.Top + this.Button0.Item.Height + spaz_vert;
            this.Matrix0.Item.Left = margine_left;
            this.Matrix0.Item.Width = this.UIAPIRawForm.Width - margine_left - margine_left;
            this.Matrix0.AutoResizeColumns();

            this.Button1.Item.Left = margine_left;
            this.Button1.Item.Top = this.Matrix0.Item.Top + this.Matrix0.Item.Height + spaz_vert;
            this.Button1.Item.Left = margine_left;

            this.UIAPIRawForm.Freeze(false);
        }

        private SAPbouiCOM.StaticText StaticText7;
        private SAPbouiCOM.ComboBox ComboBox4;
    }
}
